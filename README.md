# Robot ArUco Calibration

This package is for doing camera-to-robot calibration with an ArUco marker.

## Installation

You will need the following dependencies in your catkin workspace:

```bash
git clone git@bitbucket.org:robot-learning/simple_aruco_detector.git
git clone git@bitbucket.org:robot-learning/ll4ma_robots_description.git
```

## Marker Setup

If you need to create an ArUco marker, you can make one [here](http://chev.me/arucogen/). Set the Dictionary to be 4x4 (50, 100, 250, 1000), Marker ID as desired (default for base should be 4), and marker size as big as possible (recommended is 185mm).

### LBR4
For the LBR4, you can place the marker on the table in front of the robot base so the positive x-axis points as seen here: ![LBR4](imgs/lbr4.png)

### iiwa
For the iiwa, we've been using the following setup: 
![iiwa_front](imgs/iiwa_front.jpg)
![iiwa_bracket](imgs/iiwa_bracket.jpg)
Note we use the vertical marker since it raises it above the object table so you don't have to move that, and the vertical configuration seemed to give slightly more accurate calibrations.

## Running Calibration

Move the camera where you want it, then run the appropriate launch file based on the robot/camera you're doing calibration for. The launch files follow the convention `calibrate_<ROBOT>_<CAMERA>.launch`. So, for example, if you're using the iiwa robot and Kinect2 camera, you'll do
```bash
roslaunch robot_aruco_calibration calibrate_iiwa_kinect2.launch
```
You can list the files in `robot_aruco_calibration/launch` to see what's currently available. You may want to set `fake_joint_state:=true` if you don't have the robot publishing joint states so that you can visualize the robot model.

You should see rviz come up and see the camera feed. Press Enter when you're satisfied with the overlay in rviz, it will save the camera-to-robot transformation to a YAML configuration file (the filename should be printed to the terminal once you press Enter).

## Publishing Calibration
	
The transform from the previous step should be saved to a file in `config` folder, where it's name will have the format `<ROBOT>_<CAMERA>_calibration.yaml`. You can publish the transform with the associated launch file, for example:
```bash
roslaunch robot_aruco_calibration publish_iiwa_kinect2.launch
```

