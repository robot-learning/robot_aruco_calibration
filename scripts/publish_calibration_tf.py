#!/usr/bin/env python
import os
import rospy
import tf
import tf2_ros
import yaml
import rospkg
from geometry_msgs.msg import TransformStamped


if __name__ == '__main__':
    rospy.init_node("aruco_calibration_tf_publisher")

    filename = rospy.get_param("~calibration_file", "")
    if not filename:
        rospack = rospkg.RosPack()
        path = rospack.get_path("robot_aruco_calibration")
        filename = os.path.join(path, "config", "calibration.yaml")
    with open(filename, 'r') as f:
        poses = yaml.load(f, Loader=yaml.FullLoader)

    broadcaster = tf2_ros.StaticTransformBroadcaster()
    rate = rospy.Rate(100)
    
    rospy.loginfo("Broadcasting static TFs for scene...")
    while not rospy.is_shutdown():
        for key, pose in poses.items():
            if '__to__' not in key:
                continue
            parent_link, child_link = key.split('__to__')
            tf_stmp = TransformStamped()
            tf_stmp.header.stamp            = rospy.Time.now()
            tf_stmp.header.frame_id         = parent_link
            tf_stmp.child_frame_id          = child_link
            tf_stmp.transform.translation.x = pose["position"]["x"]
            tf_stmp.transform.translation.y = pose["position"]["y"]
            tf_stmp.transform.translation.z = pose["position"]["z"]
            tf_stmp.transform.rotation.x    = pose["orientation"]["x"]
            tf_stmp.transform.rotation.y    = pose["orientation"]["y"]
            tf_stmp.transform.rotation.z    = pose["orientation"]["z"]
            tf_stmp.transform.rotation.w    = pose["orientation"]["w"]        
            broadcaster.sendTransform(tf_stmp)
        rate.sleep()
