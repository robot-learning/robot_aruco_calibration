#!/usr/bin/env python
import os
import sys
import tf2_ros
import rospy
import rospkg

from ll4ma_util import file_util


def lookup_transform(parent, child, tf_buffer, display_msg=True):
    trans = rot = None
    if display_msg:
        rospy.loginfo("Looking up transform between '{}' and '{}'".format(parent, child))
    try:
        tf = tf_buffer.lookup_transform(parent, child, rospy.Time.now(), rospy.Duration(10.0))
        if display_msg:
            rospy.loginfo("Tranform between '{}' and '{}' found!".format(parent, child))
        return tf
    except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException) as e:
        if display_msg:
            rospy.logerr(e)
            rospy.logerr("Could not find tranform between '{}' and '{}'.".format(parent, child))
        return None


def wait_for_input(msg):
    try:
        raw_input(msg) # This is for python2.7
    except NameError:
        input(msg)


def get_pose(lookup_link, camera_link, calibration, tf_buffer):
    name = "{} ---> {}".format(lookup_link, camera_link)
    wait_for_input("\n\nPress ENTER to record TF for {}\n\n".format(name))

    tf_stmp = lookup_transform(lookup_link, camera_link, tf_buffer)
    if tf_stmp:
        rospy.loginfo("TF for {} found!".format(name))
        calibration["{}__to__{}".format(lookup_link, camera_link)] = {
            "position" : {'x' : tf_stmp.transform.translation.x,
                          'y' : tf_stmp.transform.translation.y,
                          'z' : tf_stmp.transform.translation.z},
            "orientation" : {'x' : tf_stmp.transform.rotation.x,
                             'y' : tf_stmp.transform.rotation.y,
                             'z' : tf_stmp.transform.rotation.z,
                             'w' : tf_stmp.transform.rotation.w}}
    else:
        rospy.logerr("TF for could not be found.".format(name))


if __name__ == '__main__':
    rospy.init_node("set_camera_calibration")

    camera_link = rospy.get_param("~camera_link")
    calibration_file = rospy.get_param("~calibration_file", "")
    calibrate_robot = rospy.get_param("~calibrate_robot", True)

    if not calibration_file:
        rospack = rospkg.RosPack()
        path = rospack.get_path("robot_aruco_calibration")
        calibration_file = os.path.join(path, "config/calibration.yaml")

    if os.path.exists(calibration_file):
        calibration = file_util.load_yaml(calibration_file)
    else:
        calibration = {}

    tf_buffer = tf2_ros.Buffer()
    tf_listener = tf2_ros.TransformListener(tf_buffer)
    
    rospy.loginfo("Initializing calibration node...")
    rospy.sleep(3.0)
    rospy.loginfo("Calibration node initialized")
        
    if calibrate_robot:
        # Get robot-to-camera
        base_link = rospy.get_param("~base_link")
        get_pose(base_link, camera_link, calibration, tf_buffer)
        
    rospy.loginfo("Writing transforms to file...")
    calibration['camera_link'] = camera_link
    file_util.save_yaml(calibration, calibration_file)
    rospy.loginfo("Transforms saved to file: {}".format(calibration_file))
