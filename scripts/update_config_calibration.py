#!/usr/bin/env python
import os
import sys
import yaml
import rospy
import rospkg

if __name__ == '__main__':
    """
    This is kind of a hack script to more easily update the table to base
    calibration in a config file that's in a separate package. If this
    behavior is needed more generally this script can be made more robust.
    """

    rospy.init_node("update_config_calibration")

    base_link = rospy.get_param("~base_link", "lbr4_base_link")
    table_link = rospy.get_param("~table_link", "table_center")

    rospack = rospkg.RosPack()
    path = rospack.get_path("robot_aruco_calibration")
    filename = os.path.join(path, "config/robot_camera_calibration.yaml")
    with open(filename, 'r') as f:
        data = yaml.load(f)
    pose = data["{}__to__{}".format(base_link, table_link)]

    path = rospack.get_path("ll4ma_policy_learning")
    filename = os.path.join(path, "config/table_boundary.yaml")
    with open(filename, 'r') as f:
        data = yaml.load(f)
    data["{}__to__{}".format(base_link, table_link)] = pose
    with open(filename, 'w') as f:
        yaml.dump(data, f, default_flow_style=False)
