#!/usr/bin/env python
import yaml
import rospy
import tf2_ros

from visualization_msgs.msg import MarkerArray

from ll4ma_util import ros_util
from set_camera_calibration import lookup_transform


if __name__ == '__main__':
    rospy.init_node('visualize_scene')

    camera_link = rospy.get_param("~camera_link", "")
    table_link = rospy.get_param("~table_link", "")
    table_width = rospy.get_param("~table_width", "")
    table_depth = rospy.get_param("~table_depth", "")
    table_height = rospy.get_param("~table_height", "")
    calibration_file = rospy.get_param("~calibration_file", "")

    if calibration_file:
        with open(calibration_file, 'r') as f:
            calibration = yaml.load(f, Loader=yaml.FullLoader)
        camera_link = calibration['camera_link']
        if 'table_link' in calibration:
            table_link = calibration['table_link']
            table_width = calibration['table_width']
            table_depth = calibration['table_depth']
            table_height = calibration['table_height']
                    
    tf_buffer = tf2_ros.Buffer()
    tf_listener = tf2_ros.TransformListener(tf_buffer)
    
    rospy.loginfo("Initializing scene visualization node...")
    rospy.sleep(3.0)
    rospy.loginfo("Scene visualization node initialized")

    marker_pub = rospy.Publisher("/scene_visualization", MarkerArray, queue_size=1)
    marker_array = MarkerArray()
    
    rate = rospy.Rate(100)

    scale = [table_depth, table_width, table_height]

    rospy.loginfo("Scene visualization is active")
    while not rospy.is_shutdown():
        tf_stmp = lookup_transform(camera_link, table_link, tf_buffer, False)
        if tf_stmp is not None:
            position = [tf_stmp.transform.translation.x,
                        tf_stmp.transform.translation.y,
                        tf_stmp.transform.translation.z]
            orientation = [tf_stmp.transform.rotation.x,
                           tf_stmp.transform.rotation.y,
                           tf_stmp.transform.rotation.z,
                           tf_stmp.transform.rotation.w]
            
            marker = ros_util.get_marker_msg(position, orientation, scale=scale, alpha=0.9,
                                             shape='cube', frame_id=camera_link)
            marker_array.markers = [marker]
            marker_pub.publish(marker_array)

        rate.sleep()
